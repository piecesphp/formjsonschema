class BaseSelectOptionsSchema {

	options: BaseFieldSchema[]
	name: string
	label: string
	required: boolean
	multiple: boolean
	private _elements: Element[]

	constructor(
		name: string,
		options: Array<{ value: string, display?: string }> | { value: string, display?: string },
		label: string = '',
		required: boolean = false,
	) {
		if (name === undefined) throw new Error('Falta el argumento name')
		if (typeof name != 'string') throw new TypeError('El argumento name debe ser tipo string')
		this.name = name

		options = this.clearOptions(options)

		this.options = []
		let count = 1
		for (let option of options) {
			let name = `option_${count}_${this.name}`
			let _option = new BaseFieldSchema({
				name: name,
				label: option.display,
				value: option.value,
				type: 'option'
			})
			this.options.push(_option)
			count++
		}

		if (label != undefined && typeof label != 'string') throw new TypeError('El argumento label debe ser tipo string')
		this.label = label

		if (required != undefined && typeof required != 'boolean') throw new TypeError('El argumento required debe ser tipo boolean')
		this.required = required

	}

	get elements(): Element[] {
		this.configElement()
		return this._elements
	}

	clearOptions(
		options: Array<{
			value: string,
			display?: string
		}> | {
			value: string,
			display?: string
		}
	): Array<{
		value: string,
		display?: string
	}> {
		options = Array.isArray(options) ? options : [options]
		let _options = []
		_options.push({
			value:'',
			display:'Seleccione una opción'
		})
		for (let i = 0; i < options.length; i++) {
			let option = options[i]
			if (option.value === undefined) throw new TypeError('Se esperaba que option tuviera la propiedad value')
			if (typeof option.value != 'string') throw new TypeError('La propiedad value de option debe ser tipo string')
			if (option.display != undefined && typeof option.display != 'string') throw new TypeError('La propiedad display de option debe ser tipo string')
			if (option.display == undefined || option.display.length < 1) {
				options[i].display = option.value
			}
			_options.push(options[i])
		}
		return _options
	}

	private configElement() {	
		let options = this.options
		let _options = []
		for (let option of options) {
			_options.push(option.element)
		}
		this._elements = _options
	}
}