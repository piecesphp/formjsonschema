/**
 * @class BaseFieldSchema
 * @description Esquema de un campo de formulario
 */
class BaseFieldSchema {
	id: string
	name: string
	value: boolean | number | string
	type: string
	label: string
	required: boolean
	attributes: BaseAttributeSchema[]
	private _element: Element
	protected choices: any[]
	protected tag: { tag: string, type?: string }
	static types = {
		'submit': {
			'tag': 'button',
			'type': 'submit',
		},
		'text': {
			'tag': 'input',
			'type': 'text',
		},
		'number': {
			'tag': 'input',
			'type': 'number',
		},
		'tel': {
			'tag': 'input',
			'type': 'tel',
		},
		'email': {
			'tag': 'input',
			'type': 'email',
		},
		'date': {
			'tag': 'input',
			'type': 'date',
		},
		'datetime': {
			'tag': 'input',
			'type': 'datetime',
		},
		'url': {
			'tag': 'input',
			'type': 'url',
		},
		'file': {
			'tag': 'input',
			'type': 'file',
		},
		'hidden': {
			'tag': 'input',
			'type': 'hidden',
		},
		'checkbox': {
			'tag': 'input',
			'type': 'checkbox',
		},
		'radio': {
			'tag': 'input',
			'type': 'radio',
		},
		'_checkbox': {
			'tag': 'input',
			'type': 'checkbox',
		},
		'_radio': {
			'tag': 'input',
			'type': 'radio',
		},
		'textarea': {
			'tag': 'textarea',
		},
		'option': {
			'tag': 'option',
		},
		'select': {
			'tag': 'select',
		},
		'choice-radio': {
			'tag': 'div',
		},
		'choice-checkbox': {
			'tag': 'div',
		},
	}

	/**
	 * @method constructor
	 * @description Constructor
	 * @param {Object} options
	 * @return {ThisType}
	 */
	constructor(
		options:
			{
				name: string,
				value?: boolean | number | string,
				label?: string,
				type?: string,
				required?: boolean,
				attributes?: BaseAttributeSchema | BaseAttributeSchema[]
			}
	) {

		let validOptions = this.validateOptions(options)

		this.name = validOptions.name
		this.value = validOptions.value
		this.label = validOptions.label
		this.type = validOptions.type
		this.required = validOptions.required
		this.attributes = this.validateOptionAttribute(options.attributes, validOptions.required, validOptions.name)
		this.choices = []
		this.id = this.name + '_' + Math.random().toString(36).substr(2, 9) + '_' + Date.now()
		this._element = null
	}

	get element(): Element {
		if (this._element === null) this.configElement()
		return this._element
	}
	setChoices(choices) {
		this.choices = choices
	}
	private configElement() {

		let field = <Element>document.getElementById(`${this.id}`)

		if (field === null) {

			field = document.createElement(this.tag.tag)

			field.setAttribute('id', this.id)

			let attrs = this.attributes
			for (let attr of attrs) {
				field.setAttribute(attr.name, attr.value)
			}

			if (this.type == 'select') {
				let select = this.selectOptions()
				let childs = select.elements
				let hasSelected = false
				for (let child of childs) {
					let _child = <HTMLOptionElement>child
					let value: string | string[] | number | number[] | boolean | boolean[]

					try {
						if (typeof this.value == 'string') {
							value = JSON.parse(this.value)
						} else {
							value = this.value
						}
					} catch (e) {
						value = this.value
					}

					if (!hasSelected || field.hasAttribute('multiple')) {
						if (!Array.isArray(value)) {

							if (_child.value == value && _child.value.toString().trim().length > 0) {
								_child.setAttribute('selected', '')
								hasSelected = true
							}

						} else {

							let selecting = (function (pajar, aguja) {
								for (let paja of pajar) {
									if (paja == aguja && aguja.toString().length > 0) {
										return true
									}
								}
								return false
							})(value, _child.value)

							if (selecting) {
								_child.setAttribute('selected', '')
								hasSelected = true
							}
						}
					}

					field.appendChild(_child)
				}
			} else if (this.type == 'choice-radio') {
				let choices = this.radio()
				let childs = choices.elements
				let hasSelected = false

				for (let child of childs) {
					let _child = <HTMLInputElement>child.querySelector('input')
					let value: string | string[] | number | number[] | boolean | boolean[]
					try {
						if (typeof this.value == 'string') {
							value = JSON.parse(this.value)
						} else {
							value = this.value
						}
					} catch (e) {
						value = this.value
					}
					if (!Array.isArray(value)) {
						if (_child.value == value && _child.value.toString().trim().length > 0) {
							_child.setAttribute('checked', '')
							hasSelected = true
						}

					}
					field.appendChild(child)
				}
			} else if (this.type == 'choice-checkbox') {
				let choices = this.checkbox()
				let childs = choices.elements
				let hasSelected = false

				for (let child of childs) {
					let _child = <HTMLInputElement>child.querySelector('input')
					let value: string | string[] | number | number[] | boolean | boolean[]
					try {
						if (typeof this.value == 'string') {
							value = JSON.parse(this.value)
						} else {
							value = this.value
						}
					} catch (e) {
						value = this.value
					}
					if (!Array.isArray(value)) {

						if (_child.value == value && _child.value.toString().trim().length > 0) {
							_child.setAttribute('checked', '')
							hasSelected = true
						}

					} else {

						let selecting = (function (pajar, aguja) {
							for (let paja of pajar) {
								if (paja == aguja && aguja.toString().length > 0) {
									return true
								}
							}
							return false
						})(value, _child.value)

						if (selecting) {
							_child.setAttribute('checked', '')
							hasSelected = true
						}
					}
					field.appendChild(child)
				}
			} else if (this.type == 'textarea') {
				field.innerHTML = this.value.toString().trim()
			}


			if (this.tag.type !== undefined) {
				field.setAttribute('type', this.tag.type)
			}

			if (this.type == 'submit') {
				field.innerHTML = this.label
			} else {

				let notSetName = ['option', 'choice-radio', 'choice-checkbox']
				let notSetValue = ['select','textarea']

				if (notSetValue.indexOf(this.type) == -1) {
					field.setAttribute('value', this.value.toString())
				}

				if (notSetName.indexOf(this.type) == -1) {
					field.setAttribute('name', this.name)
				}

				if (this.type == 'option') {
					field.innerHTML = this.label
				}
			}
		}

		this._element = field
	}

	private radio() {
		return new BaseFieldRadioSchema(this.name, this.getChoices(), this.label, this.required)
	}
	private checkbox() {
		return new BaseFieldCheckboxSchema(this.name, this.getChoices(), this.label, this.required)
	}
	private selectOptions() {
		return new BaseSelectOptionsSchema(this.name, this.getChoices(), this.label, this.required)
	}

	private getChoices(): Array<{ value: string, display?: string }> {
		return this.choices
	}

	/**
	 * @method validateOptions
	 * @description Valida las opciones
	 * @param options Opciones a validar
	 * @returns {object} Las opciones con valores por defecto en caso de necesitarlos
	 */
	private validateOptions(options): {
		name: string,
		value: string | number | boolean,
		label: string,
		type: string,
		required: boolean
	} {
		if (options === undefined || typeof options != 'object') {
			throw new TypeError(`Se esperaba que options fuera tipo object`)
		}

		options.value = options.value == undefined ? '' : options.value
		options.label = options.label == undefined ? '' : options.label
		options.type = options.type == undefined ? 'text' : options.type
		options.required = options.required == undefined ? false : options.required

		if (options.name === undefined || typeof options.name != 'string') {
			throw new TypeError(`Se esperaba que name fuera tipo string`)
		}

		if (Array.isArray(options.value)) {
			options.value = JSON.stringify(options.value)
		}
		if (!BaseFieldSchema.isScalar(options.value)) {
			throw new TypeError(`Se esperaba que value fuera tipo scalar`)
		}
		if (typeof options.label != 'string') {
			throw new TypeError(`Se esperaba que label fuera tipo string`)
		}
		if (typeof options.type != 'string') {
			throw new TypeError(`Se esperaba que type fuera tipo string`)
		}
		if (!BaseFieldSchema.types.hasOwnProperty(options.type)) {
			throw new TypeError(`El tipo de campo '${options.type}' no está implementado`)
		}
		if (typeof options.required != 'boolean') {
			throw new TypeError(`Se esperaba que required fuera tipo boolean`)
		}

		this.tag = BaseFieldSchema.types[options.type]

		return options
	}

	private validateOptionAttribute(attributes, isRequired, fieldName): any[] {

		attributes = attributes == undefined ? [] : attributes

		if (!BaseAttributeSchema.attrsValidate(attributes)) {
			throw new TypeError(`Se esperaba que attributes fuera tipo BaseAttributeSchema o BaseAttributeSchema[]`)
		}

		let attrs = []
		let hasRequired = false
		let hasClasses = false
		let selectClasses = [
			'ui',
			'dropdown',
			'search',
		]

		attributes = (Array.isArray(attributes) ? attributes : [attributes])

		for (let attr of attributes) {
			if (attr.name == 'class') {
				hasClasses = true
				break
			}
		}

		if (attributes.length == 0 || !hasClasses) {
			switch (this.type) {
				case 'choice-radio':
					break;
				case 'choice-checkbox':
					break;
				case 'select':
					if (!hasClasses) {
						attributes.push(new BaseAttributeSchema({ name: 'class', value: selectClasses.join(' ') }))
					}
					break;
			}
		}

		for (let attr of attributes) {

			let name = attr.name.trim()
			let value = attr.value

			switch (this.type) {
				case 'choice-radio':
					break;
				case 'choice-checkbox':
					break;
				case 'select':
					if (name == 'class') {
						value = value.trim().split(' ')
						for (let c of value) {
							if (selectClasses.indexOf(c) == -1) {
								selectClasses.push(c)
							}
						}
						value = selectClasses.join(' ')
					}
					break;
			}

			if (name == 'type') {

				if (this.tag.type !== undefined) {
					value = this.tag.type
				}

			} else if (name == 'name') {

				value = fieldName

			} else if (name == 'required') {
				hasRequired = true
				if (isRequired) {
					value = ''
				} else {
					continue
				}

			} else if (name == 'id') {
				value = this.id
			}

			let _attr = {
				name: name,
				value: value == undefined ? '' : value.trim(),
			}

			attrs.push(new BaseAttributeSchema(_attr))
		}

		if (!hasRequired) {
			if (isRequired) {
				attrs.push(new BaseAttributeSchema({ name: 'required' }))
			}
		}

		return attrs
	}

	static isField(field): field is BaseFieldSchema {
		if (field.name !== undefined) {
			return true
		}
		return false
	}
	static isFieldArray(array): array is Array<BaseAttributeSchema> {
		if (Array.isArray(array)) {
			for (let i of array) {
				if (!this.isField(i)) {
					return false
				}
			}
			return true
		}
		return false
	}
	static fieldsValidate(fields: BaseFieldSchema | Array<BaseFieldSchema>) {
		if (!this.isField(fields) && !this.isFieldArray(fields)) {
			return false
		} else {
			return true
		}
	}
	/**
	 * @method isScalar
	 * @description Valida que el valor sea escalar
	 * @param {any} value 
	 * @returns {boolean}
	 */
	static isScalar(value) {
		return /boolean|number|string/.test(typeof value);
	}
}

