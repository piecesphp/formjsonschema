class FormJsonSchema {
	static debug = false
	private _schema: BaseFormSchema
	constructor(schema: BaseFormSchema) {
		if (BaseFormSchema.isForm(schema)) {
			this._schema = new BaseFormSchema({
				fields: schema.fields,
				attributes: schema.attributes
			})
			if(FormJsonSchema.debug){
				BaseFormSchema.debug = true
			}
		} else {
			throw new Error(`Se esperaba que schema fuera un objeto BaseFormSchema`)
		}
	}
	setDebug(debug:boolean){
		debug = debug === true ? true : false
		FormJsonSchema.debug = debug
		if(FormJsonSchema.debug){
			BaseFormSchema.debug = true
		}else{
			BaseFormSchema.debug = false
		}
	}
	set schema(param){
		throw new Error('La propiedad schema es de solo lectura')
	}
	get schema(){
		return this._schema
	}
	form(): HTMLFormElement {
		return this._schema.form
	}
}
