class BaseFormSchema {
	static debug = false
	private _form: HTMLFormElement
	private validations: object
	private rules: object
	private fieldsConfigurations: object
	private fieldsValueOnEmpty: object
	private fieldsDuplicables: object
	private fieldsDuplicablesValues: object
	private fieldsDuplicablesClones: object
	private fieldsRemovibles: object
	private dependFields: object
	fields: BaseFieldSchema[]
	attributes: BaseAttributeSchema[]

	constructor(options: { fields, attributes: BaseAttributeSchema | BaseAttributeSchema[] }) {
		this.validations = {}
		this.fieldsConfigurations = {}
		this.fieldsValueOnEmpty = {}
		this.fieldsDuplicables = {}
		this.fieldsDuplicablesValues = {}
		this.fieldsDuplicablesClones = {}
		this.fieldsRemovibles = {}
		this.dependFields = {}
		this.rules = {}
		this.validateOptions(options)
		this._form = <HTMLFormElement><any>$(this.element())

		for (let field of this.fields) {
			let type = field.type
			let name = field.name

			let config: any = {}
			if (this.fieldsConfigurations[name] !== undefined) {
				config = this.fieldsConfigurations[name]
			}

			let applyCheckbox = ['choice-radio', 'choice-checkbox']
			if (applyCheckbox.indexOf(type) != -1) {
				let selector = `[name='${name}']`
				let element = this._form.find(selector)
				element.parent().checkbox(config)
			}

			if (type == 'select') {

				if (config.message == undefined) {
					config.message = {
						addResult: 'Agregar <b>{term}</b>',
						count: '{count} seleccionados',
						maxSelections: 'La cantidad máxima de selecciones es {maxCount}',
						noResults: 'No hay resultados.'
					}
				}

				let selector = `[name='${name}']`
				let element = this._form.find(selector)
				let dropdownSet = false
				let isMultiple = element.attr('multiple') !== undefined
				let selectedOptions = element.find('option[selected]')
				let countSelected = selectedOptions.length
				let hasSelected = countSelected > 0

				if (isMultiple) {
					if (hasSelected) {
						if (countSelected == 1) {
							$(document).ready(function (e) {
								element.dropdown(config)
							})
							dropdownSet = true
						}
					}
				}
				if (!dropdownSet) {
					element.dropdown(config);
				}

			}
		}
	}

	get form(): HTMLFormElement {
		return this._form
	}

	initValidations() {
		let _fields = {}
		let fields = this.fields

		let form = this._form

		for (let field of fields) {

			if (field.type == 'submit') continue;

			let validations = this.validations[field.name]

			if (validations === undefined) continue

			let rules = <any[]>validations.rules
			let prompts = <any[]>validations.prompts

			let _field: {
				identifier: string,
				rules: any[],
			} = {
				identifier: '',
				rules: []
			}

			_field.identifier = field.name

			for (let i = 0; i < rules.length; i++) {

				let rule = rules[i]
				let prompt = prompts[i]

				let _rule: {
					type: string,
					prompt?: string
				} = {
					type: '',
				}

				if (typeof rule == 'function') {

					let ruleName = '__function_rule_' + Date.now().toString()

					$.fn.form.settings.rules[ruleName] = (value) => {
						let fieldElement = form.find(`[name='${field.name}']`)
						let isFile = field.type == 'file'
						if(isFile){
							value = fieldElement[0].files
						}
						return rule(value, fieldElement)
					}

					if (prompt != null) {
						_rule.prompt = prompt
					}

					_rule.type = ruleName

				} else {

					_rule.type = rule

					if (prompt != null) {
						_rule.prompt = prompt
					}
				}


				_field.rules.push(_rule)
			}
			this.rules[field.name] = _field
			_fields[field.name] = _field

			let clones = this.fieldsDuplicablesClones[field.name]

			for (let clon of clones) {
				let _cloneField = (<any>Object).assign({}, _field)
				_cloneField.identifier = clon.name
				this.rules[clon.name] = _cloneField
				_fields[clon.name] = _cloneField
			}
		}

		let formConfig = {
			inline: true,
			onFailure: function (formErrors, fields) {

				let firstError = form.find('.field.error:first')

				try {
					$([document.documentElement, document.body]).animate({
						scrollTop: firstError.offset().top
					}, 500)
				} catch (e) {
					console.info(e)
				}

			},
			fields: _fields
		}

		form.form(formConfig)

		let dependFields = this.dependFields
		let rules = this.rules
		form.change(function (e) {
			for (let fieldName in dependFields) {
				let _dependsField = dependFields[fieldName]
				if (_dependsField.length > 0) {
					let _rules: { rules: any[] }
					if (rules[fieldName] != undefined) {
						/* for (let r of rules[fieldName].rules) {
							_rules.push(r.type)
						} */
						_rules = {
							rules: rules[fieldName].rules
						}
					} else {
						continue
					}

					let validate = true

					let _field = <JQuery>form.find(`[name='${fieldName}']`)

					for (let depend of _dependsField) {
						let dependFieldName = depend.field
						let dependFieldSelector = `[name='${dependFieldName}']`
						let _dField = form.find(dependFieldSelector)
						let isRadio = _dField.attr('type') == 'radio'
						let isCheckbox = _dField.attr('type') == 'checkbox'

						if (isRadio) {
							_dField = form.find(`${dependFieldSelector}:checked`)
						}

						let controlValue = depend.value
						let value = _dField.val()

						if (isCheckbox) {
							let name = dependFieldName.replace('[]', '')
							value = form.form('get values')[name]
							value = Array.isArray(value) ? value.filter(v => v !== false) : value
						}

						let isFunction = typeof controlValue == 'function'

						if (value == undefined) {
							validate = false
							break
						}
						if (!isFunction) {

							if (Array.isArray(value)) {

								if (value.indexOf(controlValue) == -1) {
									validate = false
									break
								}

							} else {

								if (value != controlValue) {
									validate = false
									break
								}

							}

						} else {

							validate = controlValue(value)

							if (typeof validate != 'boolean') {
								console.error('La función de validación en dependFields no ha devuelto un valor booleano')
								return
							}

							if (!validate) {
								break
							}

						}
					}


					try {
						form.form('remove fields', [fieldName])
					} catch (e) {
						console.warn(e)
					}

					if (validate) {

						form.form('add rule', fieldName, _rules)

						_field.removeAttr('schema-ignore-value')

						let container = _field.parents('.field')

						let groupedContainer = container.parents('.grouped.fields')

						if (groupedContainer.length > 0) {
							container = groupedContainer
						}

						if (container.hasClass('disabled')) {
							container.removeClass('disabled')
						}

					} else {

						_field.attr('schema-ignore-value', '')

						let container = _field.parents('.field')

						let groupedContainer = container.parents('.grouped.fields')

						if (groupedContainer.length > 0) {
							container = groupedContainer
						}

						if (!container.hasClass('disabled')) {
							container.addClass('disabled')
						}
						if (container.hasClass('error')) {
							container.removeClass('error')
						}
						container.find('.ui.prompt').remove()

						if (_field.attr('type') == 'radio' || _field.attr('type') == 'checkbox') {

							_field.parent().checkbox('uncheck')

						} else if (_field.parent().hasClass('dropdown')) {

							_field.dropdown('clear')
							_field.dropdown('refresh')

						} else {
							_field.val('')
						}
					}

				}
			}
		})

		form.change()
	}

	getValues() {
		let values = this._form.form('get values')
		let toRemove = []
		for (let name in values) {
			let isDuplicated = name.indexOf('[duplicated') !== -1
			let selector = `[name='${name}']`
			let selectorAlt = `[name*='${name}[']`
			let isArrayValue = false

			let field = <JQuery>this._form.find(selector)

			if (isDuplicated) {
				toRemove.push(name)
			}

			if (field.length == 0) {
				field = <JQuery>this._form.find(selectorAlt)
				isArrayValue = true
			}

			let isRadio = field.attr('type') == 'radio'
			let isCheckbox = field.attr('type') == 'checkbox'
			let isFile = field.attr('type') == 'file'

			if (isRadio) {
				selector = `${selector}:checked`
				field = this._form.find(selector)
			}

			let value: any = ''

			if (field.length > 0) {
				if (isArrayValue && !isCheckbox && !isRadio) {
					value = []
					field.each(function () {
						let val = $(this).val().toString().trim()
						if (val.length > 0) {
							value.push(val)
						}
					})
				} else {
					value = field.val()
				}
			}

			if (typeof value == 'string') {
				let _value = value.trim()
				if (_value.length == 0) {
					value = this.fieldsValueOnEmpty[name]
				}
			}

			if (isCheckbox) {
				value = values[name]
				if (Array.isArray(value)) {
					value = value.filter(function (val) {
						return val !== false
					})
				}
			}

			if(isFile){
				value = (<any>field[0]).files
			}

			let ignore = field.attr('schema-ignore-value') != undefined

			if (ignore) {
				value = this.fieldsValueOnEmpty[name]
			}

			values[name] = value

		}

		for (let remove of toRemove) {
			delete values[remove]
		}

		return values
	}

	/**
	 * @method setValidation
	 * @description Establece una regla de validacion para un campo
	 * @param {BaseFieldSchema} field
	 * @param {{ validation: string | FunctionStringCallback, prompt?: string }} options
	 * @returns {void}
	 */
	setValidation(field: BaseFieldSchema, options: { validation: string | FunctionStringCallback, prompt?: string }) {

		if (typeof options.validation != 'function' && typeof options.validation != 'string') {
			throw new TypeError('El parámetro validation debe ser tipo function|string')
		}

		if (options.prompt === undefined) {
			options.prompt = null
		}

		if (options.prompt !== null && typeof options.prompt != 'string') {
			throw new TypeError('El parámetro prompt debe ser tipo string')
		}

		if (this.validations[field.name] === undefined) {

			this.validations[field.name] = {
				rules: [],
				prompts: []
			}

		}

		this.validations[field.name].rules.push(options.validation)
		this.validations[field.name].prompts.push(options.prompt)

	}

	/**
	 * @method isValid
	 * @description Devuelve la validez del formulario
	 * @returns {boolean}
	 */
	isValid(): boolean {
		return this._form.form('is valid')
	}

	/**
	 * @method element
	 * @description Devuelve el formulario con todos sus campos
	 * @returns {HTMLFormElement}
	 */
	private element(): HTMLFormElement {

		let instance = this
		let form = document.createElement('form')
		let attrs = this.attributes
		let fields = Array.isArray(this.fields) ? this.fields : [this.fields]

		for (let attr of attrs) {
			form.setAttribute(attr.name, attr.value)
		}

		for (let index in fields) {
			let field = fields[index]
			this.fieldsDuplicablesClones[field.name] = []
			let duplicable = this.fieldsDuplicables[field.name]
			if (duplicable) {
				let values = <Array<any>>this.fieldsDuplicablesValues[field.name]
				let valuesNum = values.length
				for (let i = 0; i < valuesNum; i++) {
					let value = values[i]
					if (i == 0) {

						fields[index].value = value

					} else {
						let cloneField = (<any>Object).assign({}, field)
						cloneField.value = value
						let options: any = {}
						for (let property in cloneField) {
							if (field.hasOwnProperty(property)) {
								options[property] = cloneField[property]
							}
						}

						let uniqid = 'duplicated_' + Math.random().toString(36).substr(2, 9) + '_' + Date.now()
						options.name = options.name.replace('[]', '') + `[${uniqid}]`

						cloneField = new BaseFieldSchema(<any>options)
						this.fieldsDuplicablesClones[field.name].push(cloneField)
					}
				}
			}
		}

		for (let field of fields) {

			let clones = <any[]>this.fieldsDuplicablesClones[field.name]
			let hasClones = clones.length > 0
			let excludeLabels = [''] //Elementos que ignorarán el label
			let excludeWrapper = [''] //Elementos que ignorarán el contenedor
			let selfWrapper = ['choice-radio', 'choice-checkbox'] //Elementos que generan su propio contenedor
			let fieldElement = field.element //Elemento
			let wrapper = fieldElement //Contenedor, por defecto el mismo elemento 
			let groupedWrapper = document.createElement('div') //Contenedor para contenedores
			let wrapperClasses = ['field'] //Clases del contenedor
			let groupedWrapperClasses = ['grouped', 'fields'] //Clases del contenedor de grupos
			let groupedTypes = ['choice-radio', 'choice-checkbox'] //Elementos que usan contenedor de grupos
			let fileTypes = ['file'] //Elementos que se considerand el tipo file
			let shouldHasWrapper = excludeWrapper.indexOf(field.type) == -1

			if (shouldHasWrapper) {//En caso de que necesiten contenedor

				wrapper = document.createElement('div')

				let isGroupedType = groupedTypes.indexOf(field.type) != -1 //Si es de tipo grupo
				let hasSelfWrapper = selfWrapper.indexOf(field.type) != -1 //Si tiene contenedor propio
				let isFileType = fileTypes.indexOf(field.type) != -1 //Si es de tipo file

				//Verifica si usa label
				let hasLabel =
					(field.label.length > 0) &&
					(field.type != 'submit') &&
					(excludeLabels.indexOf(field.type) == -1)

				if (hasLabel) {//En caso de que use label

					let label = document.createElement('label')
					label.innerHTML = field.label
					wrapper.appendChild(label)

					if (isGroupedType) {//Si es de tipo grupos

						groupedWrapper.appendChild(label)

					} else {//Si no

						wrapper.appendChild(label)

					}

				}

				if (hasSelfWrapper) {//En caso de tener su propio contenedor

					let fieldInnerHTML = fieldElement.innerHTML
					fieldElement.innerHTML = wrapper.innerHTML + fieldInnerHTML
					wrapper = fieldElement

				} else {//Si no

					wrapper.appendChild(fieldElement)

					if (fieldElement.hasAttribute('required')) {//Se verifica si tiene el atributo required
						wrapperClasses.push('required')
					}

					wrapper.setAttribute('class', wrapperClasses.join(' '))

				}

				if (fieldElement.hasAttribute('required')) {//Se verifica si tiene el atributo required

					groupedWrapperClasses.push('required')

				}

				if (isGroupedType) {//Si es de tipo grupo
					groupedWrapper.setAttribute('class', groupedWrapperClasses.join(' '))
					groupedWrapper.appendChild(wrapper)
					wrapper = groupedWrapper
				}

				//Se verifica si es removible 
				let removible = this.fieldsRemovibles[field.name]

				if (removible) {
					let removeButton = document.createElement('button')
					removeButton.setAttribute('remove-button', '')
					removeButton.setAttribute('class', 'ui mini button red')
					removeButton.innerHTML = `<i class="trash icon"></i>Eliminar`

					if (isFileType) {
						$(field.element).click(function (e) {
							e.stopPropagation()
						})
					}

					if (hasLabel) {
						let label = wrapper.querySelector('label')
						label.innerHTML += ' ' + removeButton.outerHTML
						removeButton = label.querySelector('[remove-button]')

						$(label).click(function (e) {
							e.preventDefault()
						})
					} else {
						let firstElement = wrapper.childNodes[0]
						wrapper.insertBefore(removeButton, firstElement)
					}

					$(wrapper).click(function (e) {
						if (isFileType && e.target == field.element) {
							$(field.element).click()
						}
						e.preventDefault()
					})
					$(removeButton).click(function (e) {
						e.preventDefault()
						if (e.clientX != 0 && e.clientY != 0) {
							wrapper.remove()
						}
					})
				}

				//Se verifica si es duplicable 
				let duplicable = this.fieldsDuplicables[field.name]
				if (duplicable) {

					let fieldWrapper = wrapper.outerHTML
					let containerDuplicateButton = document.createElement('div')
					let duplicateButton = document.createElement('button')
					duplicateButton.setAttribute('duplicate-button', '')
					duplicateButton.setAttribute('class', 'ui mini button blue')
					duplicateButton.innerHTML = `<i class="plus icon"></i>Agregar otro campo "${field.label}"`
					containerDuplicateButton.appendChild(duplicateButton)
					containerDuplicateButton.setAttribute('style', ([
						'margin: 1rem 0;'
					]).join(' '))

					wrapper = document.createElement('div')
					wrapper.setAttribute('class', 'field')

					let containerFields = document.createElement('div')
					containerFields.setAttribute('duplicate-container', '')

					containerFields.innerHTML = fieldWrapper

					containerFields.querySelectorAll('[id]').forEach((element, i, parent) => {
						(parent.item(i)).removeAttribute('id')
					})

					wrapper.appendChild(containerDuplicateButton)
					wrapper.appendChild(containerFields)

					if (removible) {
						let removeButton = containerFields.querySelector('[remove-button]')
						$(removeButton).parent().click(function (e) {
							e.preventDefault()
						})
						$(removeButton).click(function (e) {
							e.preventDefault()
							let parent = $(this).parent()

							if (parent.prop('tagName') == 'LABEL') {
								parent = parent.parent()
							}
							parent.remove()
						})
					}

					let elementBase = <HTMLElement>containerFields.childNodes[0].cloneNode(true)

					$(duplicateButton).click(function (e) {
						e.preventDefault()
						instance.duplicateField(field, elementBase, containerFields, removible)

					})

					if (hasClones) {
						for (let value of clones) {
							instance.duplicateField(field, elementBase, containerFields, removible, value.value, value.name)
						}
					}
				}
			}

			form.appendChild(wrapper) //Se agrega al formulario
		}

		let hasClass = form.getAttribute('class') !== null

		let classes = []

		if (hasClass) {
			classes = form.getAttribute('class').trim().split(' ')
		}

		let hasUi = false
		let hasForm = false

		for (let _class of classes) {
			if (_class == 'ui') {
				hasUi = true
			}
			if (_class == 'form') {
				hasUi = true
			}
		}

		if (!hasUi) {
			classes.push('ui')
		}

		if (!hasForm) {
			classes.push('form')
		}

		form.setAttribute('class', classes.join(' '))

		return form
	}

	/**
	 * @method validateOptions
	 * @description Valida las opciones
	 * @param options Opciones a validar
	 * @returns {object} Las opciones con valores por defecto en caso de necesitarlos
	 */
	private validateOptions(options) {
		if (options === undefined || typeof options != 'object') {
			throw new TypeError(`Se esperaba que options fuera tipo object`)
		}
		this.validateAttributes(options.attributes)
		this.validateFields(options.fields)
	}

	/**
	 * @method validateAttributes
	 * @description Valida los atributos y defina la propiedad attributes
	 * @param {BaseAttributeSchema|BaseAttributeSchema[]} attributes atributos a validar
	 * @throws {TypeError}
	 * @returns {void}
	 */
	private validateAttributes(attributes) {

		let _attributes = []

		attributes = attributes == undefined ? null : attributes

		if (attributes !== null && !BaseAttributeSchema.attrsValidate(attributes)) {
			throw new TypeError(`Se esperaba que attributes fuera tipo BaseAttributeSchema o BaseAttributeSchema[]`)
		}

		if (attributes !== null) {
			attributes = Array.isArray(attributes) ? attributes : [attributes]
			for (let attr of attributes) {
				_attributes.push(new BaseAttributeSchema(attr))
			}
		}

		this.attributes = _attributes
	}

	/**
	 * @method validateFields
	 * @description Valida los campos de entrada y sus opciones 
	 * @param {BaseFieldSchema|BaseFieldSchema[]} fields 
	 * @throws {TypeError}
	 * @returns {void}
	 */
	private validateFields(fields) {

		if (!BaseFieldSchema.fieldsValidate(fields)) {
			throw new TypeError(`Se esperaba que fields fuera un objeto BaseFieldSchema o BaseFieldSchema[]`)
		}

		//Convierte la entrada en un array en caso de no serlo
		let fieldsParam = <Array<FieldParam>>(Array.isArray(fields) ? fields : [fields])
		//Campos a agregar
		let _fields = []

		for (let field of fieldsParam) {

			if (field.attributes === undefined) {//Prueba si tiene atributos
				field.attributes = []
			}

			//Conjunto de elementos a los que se les toma en cuenta la opción multiple
			let canBeMultiple = ['select',]

			if (canBeMultiple.indexOf(field.type) != -1) {//Prueba si puede ser múltiple
				field.multiple = field.multiple === true ? true : false
				if (field.multiple) {
					field.attributes.push(new BaseAttributeSchema({ name: 'multiple' }))
				}
			}

			let _field = new BaseFieldSchema(<any>field) //Campo

			//Conjunto de elementos que son considerados de selecciones
			let choicesFields = ['choice-radio', 'choice-checkbox', 'select',]

			if (choicesFields.indexOf(field.type) != -1) {//Valida si es un elemento con selecciones

				if (field.choices != undefined) {

					_field.setChoices(field.choices) //Agrega la selecciones

				} else {

					throw new TypeError(`Los campos tipo ${choicesFields.join('|')} deben tener la opción choices`)

				}

			}

			if (field.rules !== undefined) {//Verifica si tiene reglas

				field.rules = Array.isArray(field.rules) ? field.rules : [field.rules] //Convierte en un array, de no serlo

				for (let rule of field.rules) {

					let _rule: {
						validation: string | FunctionStringCallback,
						prompt?: string
					} = {
						validation: '',
						prompt: undefined
					}

					if (typeof rule == 'string' || typeof rule == 'function') {//En caso de que la regla sea string

						_rule.validation = rule

					} else if (typeof rule == 'object') {//En caso de que la regla sea un objeto

						_rule.validation = rule.type

						if (rule.prompt !== undefined) {//En caso de que tenga un mensaje
							_rule.prompt = rule.prompt
						}

					}

					if (_rule.validation === undefined) {//Si no cumplió las validaciones se salta a la otra
						continue
					}

					this.setValidation(_field, _rule)//Se agrega la regla
				}

			}

			//Se define un valor en caso de estar vacío el campo
			if (field.valueOnEmpty !== undefined) {

				//Si es una función se obtiene el valor
				if (typeof field.valueOnEmpty == 'function') {

					field.valueOnEmpty = field.valueOnEmpty()

				}

				//Si es un objeto se transforma en string
				if (
					typeof field.valueOnEmpty == 'symbol' ||
					typeof field.valueOnEmpty == 'object'
				) {

					field.valueOnEmpty = JSON.stringify(field.valueOnEmpty)

				}

			} else {

				field.valueOnEmpty = '' //Valor por defecto

			}

			this.fieldsValueOnEmpty[field.name] = field.valueOnEmpty

			//Se definen los campos de los que dependen el estado del campo
			if (field.dependFields !== undefined) {

				if (!Array.isArray(field.dependFields)) {
					field.dependFields = [field.dependFields]
				}

				for (let i = 0; i < field.dependFields.length; i++) {

					let _dField = field.dependFields[i]

					if (typeof _dField.field != 'string') {//Se valida el campo
						throw new TypeError(`La opcion dependFields.field debe ser tipo string`)
					}

					if (!BaseFormSchema.isScalar(_dField.value) && typeof _dField.value != 'function') {//Se valida el valor
						throw new TypeError(`La opcion dependFields.value debe ser tipo scalar o una función que retorne un bool.`)
					}

				}

			} else {
				field.dependFields = [] //Valor por defecto
			}
			this.dependFields[field.name] = field.dependFields

			//Se define si el campo es duplicable
			let duplicable = field.duplicable
			if (duplicable !== undefined) {
				if (typeof duplicable == 'boolean') {
					this.fieldsDuplicables[field.name] = duplicable

					//Verificar si ya hay valores múltiples para los campos
					_field.value = ''
					let values: any[] = field.values

					if (values === undefined) {
						values = []
					} else {
						values = Array.isArray(values) ? values : [values]
					}
					values = <Array<any>>values
					values = values.map(v => (typeof v == 'string' ? v.trim() : v))
					values = values.filter(v => (typeof v == 'string' ? (v.length > 0) : v !== undefined && v !== null))

					this.fieldsDuplicablesValues[field.name] = values;

				} else {
					throw new TypeError(`La opción duplicable debe ser tipo booleana`)
				}
			} else {
				this.fieldsDuplicables[field.name] = false //Valor por defecto
			}

			//Se define si el campo es removible
			let removible = field.removible
			if (removible !== undefined) {
				if (typeof removible == 'boolean') {
					this.fieldsRemovibles[field.name] = removible
				} else {
					throw new TypeError(`La opción removible debe ser tipo booleana`)
				}
			} else {
				this.fieldsRemovibles[field.name] = false //Valor por defecto
			}


			//Propiedades que no son validadas, sino agregadas como opciones de configuración de los
			//campos que lo requieran
			let ignoreProperties = [
				'name',
				'required',
				'label',
				'type',
				'rules',
				'attributes',
				'choices',
				'valueOnEmpty',
				'multiple',
				'dependFields',
				'duplicable',
				'removible',
				'values',
			]

			let properties = Object.getOwnPropertyNames(field)

			for (let index in properties) {
				let property = properties[index]
				if (ignoreProperties.indexOf(property) == -1) {
					if (this.fieldsConfigurations[field.name] == undefined) {
						this.fieldsConfigurations[field.name] = {}
					}
					this.fieldsConfigurations[field.name][property] = field[property]
				}
			}

			_fields.push(_field) //Se añade el campo
		}

		this.fields = _fields //Se establece la propiedad fields

	}

	/**
	 * @method isForm
	 * @description Valida que el parámetro concuerde con la estrutura de un objeto BaseFormSchema
	 * @param {Object} form 
	 * @returns {boolean}
	 */
	static isForm(form): form is BaseFormSchema {
		if (
			(form.fields !== undefined) &&
			(
				form.attributes == undefined ||
				BaseAttributeSchema.isAttribute(form.attributes) ||
				BaseAttributeSchema.isAttributeArray(form.attributes)
			)
		) {
			return true
		}
		return false
	}

	/**
	 * @method isScalar
	 * @description Valida que el valor sea escalar
	 * @param {any} value 
	 * @returns {boolean}
	 */
	static isScalar(value) {
		return /boolean|number|string/.test(typeof value);
	}

	private duplicateField(field, elementBase, containerFields, removible, value = null, name = null) {
		let newElement = <HTMLElement>elementBase.cloneNode(true)

		containerFields.appendChild(newElement)

		if (removible) {
			let removeButton = containerFields.querySelectorAll('[remove-button]')
			$(removeButton).parent().click(function (e) {
				e.preventDefault()
			})
			$(removeButton).click(function (e) {
				e.preventDefault()
				let parent = $(this).parent()

				if (parent.prop('tagName') == 'LABEL') {
					parent = parent.parent()
				}
				parent.remove()
			})
		} else {
			let removeButton = document.createElement('button')
			removeButton.setAttribute('remove-button', '')
			removeButton.setAttribute('class', 'ui mini button red')
			removeButton.innerHTML = `<i class="trash icon"></i>Eliminar`

			let hasLabel = newElement.querySelector('label') != null

			if (hasLabel) {
				let label = newElement.querySelector('label')
				label.innerHTML += ' ' + removeButton.outerHTML
				removeButton = label.querySelector('[remove-button]')

				$(label).click(function (e) {
					e.preventDefault()
				})
			} else {
				let firstElement = newElement.childNodes[0]
				newElement.insertBefore(removeButton, firstElement)
			}
			$(removeButton).click(function (e) {
				e.preventDefault()
				newElement.remove()
			})

		}
		let _field = $(newElement).find(`[name='${field.name}']`)
		if (name !== null) {
			_field.attr('name', name)
		}
		_field.val('')
		if (value !== null) {
			_field.val(value)
		}
		if (this._form !== undefined) this.initValidations()
	}

}

interface FieldParam {
	name: string
	required?: boolean
	label?: string
	type?: string
	rules?: any
	attributes?: Array<{ name: string, value?: string }>
	choices?: Array<{ value: string, display?: string }>
	valueOnEmpty?: any
	multiple?: boolean
	dependFields?: Array<{ field: string, value: any }>
	duplicable?: boolean
	removible?: boolean
	values?: any
}