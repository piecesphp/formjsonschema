class BaseAttributeSchema {
	name: string
	value: string
	constructor(options: { name: string, value?: string }) {
		options = this.validateOptions(options)
		this.name = options.name
		this.value = options.value
	}

	/**
	 * @method validateOptions
	 * @description Valida las opciones
	 * @param options Opciones a validar
	 * @returns {object} Las opciones con valores por defecto en caso de necesitarlos
	 */
	private validateOptions(options) {
		if (options === undefined || typeof options != 'object') {
			throw new TypeError(`Se esperaba que options fuera tipo object`)
		}

		options.value = options.value == undefined ? '' : options.value

		if (options.name === undefined || typeof options.name != 'string') {
			throw new TypeError(`Se esperaba que name fuera tipo string`)
		}
		if (typeof options.value != 'string') {
			throw new TypeError(`Se esperaba que value fuera tipo string`)
		}

		options.name = options.name.trim()
		options.value = options.value.trim()

		return options
	}
	static isAttribute(attr): attr is BaseAttributeSchema {
		if (attr.name !== undefined && typeof attr.name == 'string') {
			return true
		}
		return false
	}
	static isAttributeArray(array): array is Array<BaseAttributeSchema> {
		if (Array.isArray(array)) {
			for (let i of array) {
				if (!this.isAttribute(i)) {
					return false
				}
			}
			return true
		}
		return false
	}
	static attrsValidate(attrs: BaseAttributeSchema | Array<BaseAttributeSchema>) {
		if (!this.isAttribute(attrs) && !this.isAttributeArray(attrs)) {
			return false
		} else {
			return true
		}
	}
}
