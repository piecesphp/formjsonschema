class BaseFieldRadioSchema {

	options: BaseFieldSchema[]
	name: string
	label: string
	required: boolean
	private _elements: Element[]

	constructor(
		name: string,
		options: Array<{ value: string, display?: string }> | { value: string, display?: string },
		label: string = '',
		required: boolean = false
	) {
		if (name === undefined) throw new Error('Falta el argumento name')
		if (typeof name != 'string') throw new TypeError('El argumento name debe ser tipo string')
		this.name = name

		options = this.clearOptions(options)

		this.options = []

		for (let option of options) {
			let _option = new BaseFieldSchema({
				name: this.name,
				label: option.display,
				value: option.value,
				type: '_radio'
			})
			this.options.push(_option)
		}

		if (label != undefined && typeof label != 'string') throw new TypeError('El argumento label debe ser tipo string')
		this.label = label

		if (required != undefined && typeof required != 'boolean') throw new TypeError('El argumento label debe ser tipo boolean')
		this.required = required
	}

	get elements(): Element[] {
		this.configElements()
		return this._elements
	}

	clearOptions(
		options: Array<{
			value: string,
			display?: string
		}> | {
			value: string,
			display?: string
		}
	): Array<{
		value: string,
		display?: string
	}> {
		options = Array.isArray(options) ? options : [options]
		for (let i = 0; i < options.length; i++) {
			let option = options[i]
			if (option.value === undefined) throw new TypeError('Se esperaba que option tuviera la propiedad value')
			if (typeof option.value != 'string') throw new TypeError('La propiedad value de option debe ser tipo string')
			if (option.display != undefined && typeof option.display != 'string') throw new TypeError('La propiedad display de option debe ser tipo string')
			if (option.display == undefined || option.display.length < 1) {
				options[i].display = option.value
			}
		}
		return options
	}

	private configElements() {

		let options = this.options
		let _options = []
		let wrapperClasses = ['field']

		if(this.required){
			//wrapperClasses.push('required')
		}
		
		for (let option of options) {

			let wrapper = document.createElement('div')
			let inputWrapper = document.createElement('div')

			inputWrapper.setAttribute('class', 'ui slider checkbox')
			inputWrapper.appendChild(option.element)

			if (option.label.length > 0) {
				let label = document.createElement('label')
				label.innerHTML = option.label
				inputWrapper.appendChild(label)
			}

			wrapper.appendChild(inputWrapper)		

			wrapper.setAttribute('class',wrapperClasses.join(' '))

			_options.push(wrapper)
		}
		this._elements = _options
	}
}