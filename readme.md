#FormJsonSchema
- Generador de formularios a partir de un esquema
- Nota: necesita usarse junto con semantic-ui y jquery

##API

###class FormJsonSchema(schema)
```ts
 schema:{
    attributes?:Array<{
        name:string,
        value?:string
    }>|{
        name:string,
        value?:string
    },
    fields:Array<{
        name:string,
        value?:array|number|string,//If duplicable is false
        values?:array<array|number|string>,//If duplicable is true
        required?:boolean,
        valueOnEmpty?:any,
        label?:string,
        type?:string,
        dependFields?:Array<{
            field:scalar,
            value:string|Function:boolean
        }>|{
            field:scalar,
            value:string
        },
        rules?:Array<{
            type:string,
            prompt?:string
        }>|{
            type:string,
            prompt?:string
        },
        attributes?:Array<{
            name:string,
            value?:string
        }>|{
            name:string,
            value?:string
        },
        choices?:Array<{
            value:string,
            display?:string
        }>|{
            value:string,
            display?:string
        },
	    duplicable?: boolean,
	    removible?: boolean
    }>
 }
```
####Methods
- initValidations:void
- form:JQuery<HTMLFormElement>
- getValues:Object
- isValid:boolean


##Install

npm install vmorantes-form-json-schema

## Ejemplo
```js
$(document).ready(function (e) {

	let formSchema = {
		attributes: [
			{
				name: 'method',
				value: 'POST',
			},
			{
				//Importante para que el navegador no haga las validaciones nativas
				//Otra opción es agregar este atributo directamente al elemento deseado
				name: 'novalidate',
				value: 'true',
			}
		],
		fields: [
			{
				name: 'test1',
				value: '',
				required: true,
				removible: true,
				label: 'Campo numérico',
				type: 'number',
				rules: [
					{
						type: 'empty',
					},
				],
				attributes: [
					{
						name: 'test',
						value: 'value-test'
					}
				]
			},
			{
				name: 'test2',
				value: '',
				label: 'No validation',
				type: 'number',
			},
			{
				name: 'test-checkbox[]',
				required: true,
				removible: true,
				value: [1,2],
				label: 'Checkbox',
				type: 'choice-checkbox',
				rules: [
					{
						type: 'checked',
					},
				],
				choices: [
					{
						value: '0',
						display: 'Verde'
					},
					{
						value: '1',
						display: 'Azul'
					},
					{
						value: '2',
						display: 'Rojo'
					}
				]

			},
			{
				name: 'test-radio',
				required: true,
				value: '0',
				label: 'Radio (Its depedens on Checkbox: Rojo)',
				type: 'choice-radio',
				dependFields: [
					{
						field: 'test-checkbox[]',
						value: '2'
					}
				],
				rules: [
					{
						type: 'checked',
					},
				],
				choices: [
					{
						value: '0',
						display: 'Si'
					},
					{
						value: '1',
						display: 'No'
					}
				]

			},
			{
				name: 'test-dependiente-1',
				label: 'Select (Its depedens on Radio: Si)',
				type: 'select',
				valueOnEmpty: null,
				dependFields: [
					{
						field: 'test-radio',
						value: function (value) {
							return value == 0
						}
					}
				],
				rules: [
					{
						type: 'empty',
						prompt: 'Si test-radio = 0 este campo es obligatorio',
					},
				],
				choices: [
					{
						value: '0',
						display: 'Si'
					},
					{
						value: '1',
						display: 'No'
					}
				]

			},
			{
				name: 'test-dependiente-2',
				label: 'Select2 (Its depedens on Select1: Si)',
				type: 'select',
				value: 0,
				valueOnEmpty: null,
				dependFields: [
					{
						field: 'test-dependiente-1',
						value: '0'
					}
				],
				rules: [
					{
						type: 'empty',
						prompt: 'Si test-dependiente-1 = 0 este campo es obligatorio',
					},
				],
				choices: [
					{
						value: '0',
						display: 'Si'
					},
					{
						value: '1',
						display: 'No'
					}
				]

			},
			{
				name: 'test-select-multiple',
				label: 'Select multiple',
				type: 'select',
				value: [0,3],
				multiple:true,
				rules: [
					{
						type: 'empty',
						prompt: 'Si test-dependiente-1 = 0 este campo es obligatorio',
					},
				],
				choices: [
					{
						value: '0',
						display: 'A'
					},
					{
						value: '1',
						display: 'B'
					},
					{
						value: '2',
						display: 'C'
					},
					{
						value: '3',
						display: 'D'
					},
				]

			},
			{
				name: 'test-duplicable[]',
				label: 'Duplicable',
				type: 'text',
				values:[1,2,3,4,5,5,6,7,8,9,10,0,513,'','   ','   test    ',[]],
				duplicable: true,
				removible: false,
				rules: [
					{
						type: 'empty',
					},
				],
			},
			{
				name: '',
				attributes: [
					{
						name: 'class',
						value: 'ui button green',
					},
				],
				label: 'Enviar',
				type: 'submit'
			}
		]
	}

	let jsonToForm = new FormJsonSchema(formSchema)//Instancia el esquematizador

	jsonToForm.schema.initValidations() //Configura las validaciones

	let form = jsonToForm.form() //Obtiene el formulario

	$('.test').html(form) //Inserta el formulario

	form.submit(function (e) {

		e.preventDefault()

		let data = jsonToForm.schema.getValues() //Obtiene los valores

		console.log(data)

		if (jsonToForm.schema.isValid()) {//Verifica la validez

			console.log('VALIDO')

		} else {

			console.log('INVALIDO')

		}


		return false
	})

})

```