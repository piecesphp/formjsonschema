#FormJsonSchema
- Generador de formularios a partir de un esquema
- Nota: necesita usarse junto con semantic-ui y jquery

##API

###class FormJsonSchema(schema)
```ts
 schema:{
    attributes?:Array<{
        name:string,
        value?:string
    }>|{
        name:string,
        value?:string
    },
    fields:Array<{
        name:string,
        value?:array|number|string,//If duplicable is false
        values?:array<array|number|string>,//If duplicable is true
        required?:boolean,
        valueOnEmpty?:any,
        label?:string,
        type?:string,
        dependFields?:Array<{
            field:scalar,
            value:string|Function:boolean
        }>|{
            field:scalar,
            value:string
        },
        rules?:Array<{
            type:string,
            prompt?:string
        }>|{
            type:string,
            prompt?:string
        },
        attributes?:Array<{
            name:string,
            value?:string
        }>|{
            name:string,
            value?:string
        },
        choices?:Array<{
            value:string,
            display?:string
        }>|{
            value:string,
            display?:string
        },
	    duplicable?: boolean,
	    removible?: boolean
    }>
 }
```
####Methods
- initValidations:void
- form:JQuery<HTMLFormElement>
- getValues:Object
- isValid:boolean


##Install

npm install vmorantes-form-json-schema

## Ejemplo
```js
<%= example %>
```