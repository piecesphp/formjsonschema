$(document).ready(function (e) {

	let fields = []
	fields.push({
		name: 'test2',
		value: '',
		label: 'No validation',
		type: 'number',
	})
	fields.push({
		name: 'test-checkbox[]',
		required: true,
		removible: true,
		value: [1, 2],
		label: 'Checkbox',
		type: 'choice-checkbox',
		rules: [
			{
				type: 'checked',
			},
		],
		choices: [
			{
				value: '0',
				display: 'Verde'
			},
			{
				value: '1',
				display: 'Azul'
			},
			{
				value: '2',
				display: 'Rojo'
			}
		]

	})
	fields.push({
		name: 'test-radio',
		required: true,
		value: 0,
		label: 'Radio (Its depedens on Checkbox: Rojo)',
		type: 'choice-radio',
		dependFields: [
			{
				field: 'test-checkbox[]',
				value: '2'
			}
		],
		rules: [
			{
				type: 'checked',
			},
		],
		choices: [
			{
				value: '0',
				display: 'Si'
			},
			{
				value: '1',
				display: 'No'
			}
		]

	})
	fields.push({
		name: 'test-dependiente-1',
		label: 'Select (Its depedens on Radio: Si)',
		type: 'select',
		valueOnEmpty: null,
		dependFields: [
			{
				field: 'test-radio',
				value: function (value) {
					return value == 0
				}
			}
		],
		rules: [
			{
				type: 'empty',
				prompt: 'Si test-radio = 0 este campo es obligatorio',
			},
		],
		choices: [
			{
				value: '0',
				display: 'Si'
			},
			{
				value: '1',
				display: 'No'
			}
		]

	})
	fields.push({
		name: 'test-dependiente-2',
		label: 'Select2 (Its depedens on Select1: Si)',
		type: 'select',
		value: 0,
		valueOnEmpty: null,
		dependFields: [
			{
				field: 'test-dependiente-1',
				value: '0'
			}
		],
		rules: [
			{
				type: 'empty',
				prompt: 'Si test-dependiente-1 = 0 este campo es obligatorio',
			},
		],
		choices: [
			{
				value: '0',
				display: 'Si'
			},
			{
				value: '1',
				display: 'No'
			}
		]

	})
	fields.push({
		name: 'test-select-multiple',
		label: 'Select multiple',
		type: 'select',
		value: [0, 3],
		multiple: true,
		rules: [
			{
				type: 'empty',
				prompt: 'Si test-dependiente-1 = 0 este campo es obligatorio',
			},
		],
		choices: [
			{
				value: '0',
				display: 'A'
			},
			{
				value: '1',
				display: 'B'
			},
			{
				value: '2',
				display: 'C'
			},
			{
				value: '3',
				display: 'D'
			},
		]

	})
	fields.push({
		name: 'test-duplicable[]',
		label: 'Duplicable',
		type: 'text',
		values: [1, 2, 3, '', '   ', '   test    ', []],
		duplicable: true,
		removible: true,
		rules: [
			{
				type: 'empty',
			},
		],
	})
	fields.push({
		name: 'test1',
		value: '',
		required: true,
		removible: true,
		label: 'Campo numérico',
		type: 'number',
		rules: [
			{
				type: 'empty',
			},
		],
		attributes: [
			{
				name: 'test',
				value: 'value-test'
			}
		]
	})
	fields.push({
		name: 'test1',
		value: '',
		required: true,
		removible: true,
		label: 'Campo numérico',
		type: 'number',
		rules: [
			{
				type: 'empty',
			},
		],
		attributes: [
			{
				name: 'test',
				value: 'value-test'
			}
		]
	})
	fields.push({
		name: 'textarea',
		value: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatibus esse tempore soluta deserunt eligendi, libero inventore nobis dicta quasi, quam sit sed facilis accusantium tempora, doloribus eveniet animi consectetur magnam!',
		required: true,
		removible: true,
		label: 'Textarea',
		type: 'textarea',
		rules: [
			{
				type: 'empty',
			},
		],
	})
	fields.push({
		name: 'file',
		value: 'archivoasfmaspfmas',
		required: true,
		removible: true,
		label: 'File',
		type: 'file',
		rules: [
			{
				type: function (value, input) {
					console.log(value)
					return input[0].files.length > 0
				},
				prompt: 'Debe ingresar un archivo'
			},
		],
	})
	fields.push({
		name: '',
		attributes: [
			{
				name: 'class',
				value: 'ui button green',
			},
		],
		label: 'Enviar',
		type: 'submit'
	})
	let formSchema = {
		attributes: [
			{
				name: 'method',
				value: 'POST',
			},
			{
				name: 'enctype',
				value: 'multipart/form-data',
			},
			{
				//Importante para que el navegador no haga las validaciones nativas
				//Otra opción es agregar este atributo directamente al elemento deseado
				name: 'novalidate',
				value: 'true',
			}
		],
		fields: fields
	}

	let jsonToForm = new FormJsonSchema(formSchema)//Instancia el esquematizador

	jsonToForm.schema.initValidations() //Configura las validaciones

	let form = jsonToForm.form() //Obtiene el formulario

	$('.test').html(form) //Inserta el formulario

	form.submit(function (e) {

		e.preventDefault()

		let data = jsonToForm.schema.getValues() //Obtiene los valores

		console.log(data)

		if (jsonToForm.schema.isValid()) {//Verifica la validez

			console.log('VALIDO')

		} else {

			console.log('INVALIDO')

		}


		return false
	})

})
