/*
* Dependencias
*/
var gulp = require('gulp')
var typescript = require('gulp-typescript')
var watch = require('gulp-watch')
var sourcemaps = require('gulp-sourcemaps')
var concat = require('gulp-concat')
var uglify = require('gulp-uglify')

gulp.task('compile', function () {
	compile()
})

gulp.task('watch', function () {
	return watch(['src/*.ts'], () => {
		compile()
	})
})


function compile() {

	console.time('Duración de la compilación')

	console.log('Compilando..')

	var typescriptProject = typescript.createProject('tsconfig.json')

	typescriptProject.src()
		.pipe(sourcemaps.init())
		.pipe(typescriptProject()).js
		.pipe(sourcemaps.write('./'))
		.pipe(gulp.dest('./dist'))
		.on('end', () => {
			console.timeEnd('Duración de la compilación')
		})
}